<?php

/**
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2014
 * @package yii2-widgets
 * @subpackage yii2-widget-sidenav
 * @version 1.0.0
 */

namespace app\components;

use Yii;

class LanguageHandler extends \yii\base\Behavior
{
    public function events()
    {
        return [\yii\web\Application::EVENT_BEFORE_REQUEST => 'handleBeginRequest'];
    }

    public function handleBeginRequest($event)
    {
        if(Yii::$app->session->has('language'))
        {
            Yii::$app->language = Yii::$app->session->get('language');
        }else{
            $session = Yii::$app->session;
            $session->set('language', 'en');
        }
    }
}
