<p align="center">
    <h1 align="center">CMSOST Basic v1.0</h1>
    <br>
</p>

CMSOST Basic v1.0 is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.

The template contains the basic features including user login/logout.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      components/         contains widgets classes
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      migrations/         contains database migration files
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      themes/             contaons theme using by system
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources

Quick Start
-----------
1. Clone this repo
2. Run ```composer install```
3. Create a database
4. Copy ```config/console_example.php``` to ```config/console.php```
5. Copy ```config/db_example.php``` to ```config/db.php```
6. Copy ```config/web_example.php``` to ```config/web.php```
7. Change ```config/db.php``` database connection with the newly created database
8. Run ```yii autocrud``` command


### Default user
Username: root
Password: O$hb0t_831

TODO
-----------
- [x] Third level menu support
- [ ] Multiple url support to active menu
- [ ] Audit trail menu
- [ ] Parameter support multiple language
- [ ] Store and view last user login
- [ ] Confirmation before logout