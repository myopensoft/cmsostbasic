<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%sys_ref}}`.
 */
class m171118_090002_create_table_sys_ref extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%sys_ref}}', [

            'id' => $this->primaryKey()->notNull(),
            'cat' => $this->string(25),
            'code' => $this->string(25),
            'descr_ms' => $this->string(150),
            'descr_en' => $this->string(150),
            'sort' => $this->integer(11),
            'param' => $this->string(5),
            'status' => $this->char(1)->notNull(),
            'created_by' => $this->string(50),
            'created_at' => $this->datetime(),
            'updated_at' => $this->datetime(),
            'updated_by' => $this->string(50),

        ]);
     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%sys_ref}}');
    }
}
