<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%source_message}}`.
 */
class m171118_090003_create_table_source_message extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%source_message}}', [

            'id' => $this->primaryKey()->notNull(),
            'category' => $this->string(255),
            'message' => $this->text(),

        ],$tableOptions);
     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%source_message}}');
    }
}
