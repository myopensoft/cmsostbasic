<?php

use yii\db\Migration;

/**
 * Handles the data insertion for table `{{%sys_menu_nested}}`.
 */
class m190722_090111_insert_data_into_sys_menu_nested extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        
        $this->batchInsert('{{%sys_menu_nested}}', ['id','root','lft','rgt','lvl','name','icon','icon_type','active','selected','disabled','readonly','visible','collapsed','movable_u','movable_d','movable_l','movable_r','removable','removable_all','child_allowed','menu_loc','controller_name','action_name','remarks','created_by','created_at','updated_by','updated_at'], [
			['1','1','1','16','0','Menu','','1','1','0','0','0','1','1','1','1','1','1','1','0','1','','','','','','','',''],
			['2','1','2','15','1','Administrator','th','1','1','0','0','0','1','1','1','1','1','1','1','0','1','','','','','','','',''],
			['3','1','3','4','2','Manage Parameter','','1','1','0','0','0','1','1','1','1','1','1','1','0','1','ref/index-cat','ref','index-cat','','','','',''],
			['4','1','5','6','2','Manage Translation','','1','1','0','0','0','1','1','1','1','1','1','1','0','1','translation/index','translation','index','','','','',''],
			['5','1','7','8','2','Manage Route','','1','1','0','0','0','1','1','1','1','1','1','1','0','1','mimin/route/index','route','index','','','','',''],
			['6','1','9','10','2','Manage Role','','1','1','0','0','0','1','1','1','1','1','1','1','0','1','mimin/role/index','role','index','','','','',''],
			['7','1','11','12','2','Manage Users','','1','1','0','0','0','1','1','1','1','1','1','1','0','1','user/index','user','index','','','','',''],
			['8','1','13','14','2','Manage Menu','','1','1','0','0','0','1','0','1','1','1','1','1','0','1','menu-nested/index','menu-nested','index','','','','','']]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
               
$this->truncateTable('{{%sys_menu_nested}}');
    }
}
