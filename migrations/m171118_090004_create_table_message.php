<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%message}}`.
 */
class m171118_090004_create_table_message extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%message}}', [

            'id' => $this->integer(11)->notNull(),
            'language' => $this->string(16)->notNull(),
            'translation' => $this->text(),

        ],$tableOptions);
 
        // creates index for column `id`
        $this->createIndex(
            'fk_message_source_message',
            '{{%message}}',
            'id'
        );

        // add foreign key for table `source_message`
        $this->addForeignKey(
            'fk_message_source_message',
            '{{%message}}',
            'id',
            '{{%source_message}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `source_message`
        $this->dropForeignKey(
            'fk_message_source_message',
            '{{%message}}'
        );

        // drops index for column `id`
        $this->dropIndex(
            'fk_message_source_message',
            '{{%message}}'
        );

        $this->dropTable('{{%message}}');
    }
}
