<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%sys_user}}`.
 */
class m171118_090001_create_table_sys_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        
        $this->createTable('sys_user', [

            'id' => $this->primaryKey()->notNull(),
            'username' => $this->string(255)->notNull(),
            'full_name' => $this->string(255),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string(255)->notNull(),
            'password_reset_token' => $this->string(255),
            'verification_token' => $this->string()->defaultValue(null),
            'email' => $this->string(255)->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(10),
            'created_at' => $this->datetime(),
            'updated_at' => $this->datetime(),
            'created_by' => $this->string(25),
            'updated_by' => $this->string(25),

        ],$tableOptions);
     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%sys_user}}');
    }
}
