<?php

use yii\db\Migration;

/**
 * Class m190710_060750_insert_data_into_source_message
 */
class m190710_060750_insert_data_into_source_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%source_message}}', ['id','category','message'], [
			['1','btn','Search'],
			['2','btn','Reset'],
			['3','btn','Back'],
			['4','btn','Save'],
			['5','btn','Update'],
			['6','sweetdelete','titleconfirm'],
			['7','sweetdelete','textconfirm'],
			['8','sweetdelete','titlesuccess'],
			['9','sweetdelete','textsuccess'],
			['10','tooltip','Update'],
			['11','tooltip','Delete']]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("SET foreign_key_checks = 0;");
        $this->truncateTable('{{%source_message}}');
        $this->execute("SET foreign_key_checks = 1;");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190710_060750_insert_data_into_source_message cannot be reverted.\n";

        return false;
    }
    */
}
