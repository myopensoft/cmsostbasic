<?php

use yii\db\Migration;

/**
 * Class m190710_060940_insert_data_into_message
 */
class m190710_060940_insert_data_into_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%message}}', ['id','language','translation'], [
			['1','en','Search'],
			['1','ms','Carian'],
			['2','en','Reset'],
			['2','ms','Semula'],
			['3','en','Back'],
			['3','ms','Kembali'],
			['4','en','Save'],
			['4','ms','Simpan'],
			['5','en','Update'],
			['5','ms','Kemaskini'],
			['6','en','Are you sure?'],
			['6','ms','Adakah anda pasti?'],
			['7','en','You will not be able to recover this imaginary file!'],
			['7','ms','Anda tidak akan dapat memulihkan fail ini selepas dihapuskan!'],
			['8','en','Deleted!'],
			['8','ms','Dihapuskan!'],
			['9','en','Your imaginary file has been deleted'],
			['9','ms','Rekod telah berjaya dihapuskan'],
			['10','ms','Kemaskini'],
			['10','en','Update'],
			['11','ms','Hapus'],
			['11','en','Delete']]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable('{{%message}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190710_060940_insert_data_into_message cannot be reverted.\n";

        return false;
    }
    */
}
