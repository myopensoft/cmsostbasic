<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%sys_menu_nested}}`.
 */
class m190722_090102_create_table_sys_menu_nested extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%sys_menu_nested}}', [

            'id' => $this->primaryKey()->notNull(),
            'root' => $this->integer(11),
            'lft' => $this->integer(11)->notNull(),
            'rgt' => $this->integer(11)->notNull(),
            'lvl' => $this->smallInteger(5)->notNull(),
            'name' => $this->string(60)->notNull(),
            'icon' => $this->string(255),
            'icon_type' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'active' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'selected' => $this->tinyInteger(1)->notNull(),
            'disabled' => $this->tinyInteger(1)->notNull(),
            'readonly' => $this->tinyInteger(1)->notNull(),
            'visible' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'collapsed' => $this->tinyInteger(1)->notNull(),
            'movable_u' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'movable_d' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'movable_l' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'movable_r' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'removable' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'removable_all' => $this->tinyInteger(1)->notNull(),
            'child_allowed' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'menu_loc' => $this->string(100),
            'controller_name' => $this->string(100),
            'action_name' => $this->string(100),
            'remarks' => $this->string(100),
            'created_by' => $this->integer(11),
            'created_at' => $this->datetime(),
            'updated_by' => $this->integer(11),
            'updated_at' => $this->datetime(),

        ]);
     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%sys_menu_nested}}');
    }
}
