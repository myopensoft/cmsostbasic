<?php
// print_r($generator);
use yii\helpers\Inflector;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();
echo "<?php\n";
?>

use yii\helpers\Html;
use kartik\export\ExportMenu;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use <?= $generator->indexWidgetType === 'grid' ? "kartik\\grid\\GridView" : "yii\\widgets\\ListView" ?>;
<?= $generator->enablePjax ? 'use yii\widgets\Pjax;' : '' ?>

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('breadcrumbs_<?= $generator->messageCategory ?>','<?= Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>');
$this->params['breadcrumbs'][] = $this->title;

$urlCreate = Url::to(['create']);
$urlUpdate = Url::to(['update','id'=>'']);

$jsBegin = <<< JS
    function create() {
        $('#modalCreate').modal('show').find('#modalCreateContent').load('$urlCreate');
        return false;
    }
    function update(id) {
        $('#modalUpdate').modal('show').find('#modalUpdateContent').load('$urlUpdate' + id);
        return false;
    }
    
JS;
$this->registerJs($jsBegin, yii\web\View::POS_BEGIN);

?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index tooltip-demo">

<?= $generator->enablePjax ? "    <?php Pjax::begin(['id'=>'indexpjax','timeout'=>3600000,'enablePushState'=>false]); ?>\n" : '' ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-search"></i><?= " <?= Yii::t('titlepanel', 'Search') ?>\n" ?>
        </div>
        <div class="panel-body">
            <?php if(!empty($generator->searchModelClass)): ?>
<?= "<?php " . ($generator->indexWidgetType === 'grid' ? "" : "") ?>echo $this->render('_search', ['model' => $searchModel]); ?>
        <?php endif; ?></div>
    </div>

    <?= "<?php\n        \$gridColumns = [\n" ?>
            ['class' => 'yii\grid\SerialColumn'],

<?php
$count = 0;
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        if (++$count < 6) {
            echo "            '" . $name . "',\n";
        } else {
            echo "            //'" . $name . "',\n";
        }
    }
} else {
    foreach ($tableSchema->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        if (++$count < 6) {
            echo "            '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        } else {
            echo "            //'" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
        }
    }
}
?>

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'header' => 'Action',
                'contentOptions'=>['style'=>'width: 10%;'],
                'buttons' => [
                   'update' => function ($url,$model) {
                       return Html::button('<i class="fa fa-pencil-alt"></i>', [
                           'class' => 'btn btn-success btn-xs btn-rounded btn-outline',
                           'data-toggle' => 'tooltip',
                           'data-placement' => 'bottom',
                           'title' => Yii::t('tooltip', 'Update'),
                           'onClick'=>"update($model->id)",
                       ]);
                   },
                   'delete' => function ($url) {
                        $sweetdeleteconfirmtitle = Yii::t('sweetdelete', 'titleconfirm');
                        $sweetdeleteconfirmtext = Yii::t('sweetdelete', 'textconfirm');
                        $sweetdeletesuccesstitle = Yii::t('sweetdelete', 'titlesuccess');
                        $sweetdeletesuccesstext = Yii::t('sweetdelete', 'textsuccess');
                        $delete = Yii::$app->params['successDelete'];
                        return Html::button('<i class="fa fa-trash"></i>', [
                            'class' => 'btn btn-danger btn-xs btn-rounded btn-outline',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'bottom',
                            'title' => Yii::t('tooltip', 'Delete'),
                            'onclick' => "
                                swal({
                                    title: '$sweetdeleteconfirmtitle',
                                    text: '$sweetdeleteconfirmtext',
                                    type: 'warning',
                                    showCancelButton: true,
                                    cancelButtonText: 'No',
                                    cancelButtonClass: 'btn-primary btn-rounded btn-outline',
                                    confirmButtonText: 'Yes',
                                    confirmButtonClass: 'btn-danger btn-rounded btn-outline',
                                    closeOnConfirm: false
                                }, function () {
                                    $.ajax('$url', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        //$.pjax.reload({container: '#indexpjax'});
                                        $.pjax.reload('#indexpjax', {timeout : false});
                                        toastr.success('$delete');
                                        swal({
                                        title: '$sweetdeletesuccesstitle',
                                        text: '$sweetdeletesuccesstext',
                                        type: 'success',
                                        confirmButtonClass: 'btn-primary btn-rounded btn-outline',
                                        });
                                    });
                                });
                            ",
                        ]);
                    },
                ]
            ],
        ];

        $fullExportMenu =  
        Html::button('<i class="fa fa-plus"></i> '.Yii::t('btnCreate', '<?= Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>'), ['class' => 'btn btn-success btn-sm btn-rounded btn-outline', 'onClick'=>"create()",]).
        ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_BLANK,
            'fontAwesome' => true,
            'columnSelectorOptions'=>[
                'label' => 'Columns',
                'class' => 'btn btn-default btn-sm btn-rounded btn-outline'
            ],
            'dropdownOptions' => [
                'label' => 'Full',
                'class' => 'btn btn-default btn-sm btn-rounded btn-outline',
                'itemsBefore' => [
                    '<li class="dropdown-header">Export All Data</li>',
                ],
            ],
            'pjaxContainerId' => 'kv-pjax-container',
        ]);
    ?>
<?php if ($generator->indexWidgetType === 'grid'): ?>
    <?= "<?= " ?>GridView::widget([
        'dataProvider' => $dataProvider,
        <?= !empty($generator->searchModelClass) ? "//'filterModel' => \$searchModel,\n        'columns' => \$gridColumns,\n" : "'columns' => \n"; ?>
        'toolbarContainerOptions' => ['class' => 'btn-toolbar kv-grid-toolbar toolbar-container float-left'],
        'pjax' => false,
        'panel' => [
            //'type' => GridView::TYPE_SUCCESS,
            'heading' => '<i class="fa fa-bars"></i> '.Yii::t('titlepanel', 'List'),
        ],
        // set a label for default menu
        'export' => [
            'label' => 'Page',
            'fontAwesome' => true,
            'options' => ['class' => 'btn btn-default btn-sm btn-rounded btn-outline'],
        ],
        'toolbar' => [
            $fullExportMenu,
            '{export}',
        ],
        'responsive' => true,
        'responsiveWrap' => false,    
        
    ]); ?>
    <?= "<?php\n" ?>
        Modal::begin([
            'title' => Yii::t('modaltitle','<?= 'Create'.Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>'),
            'id' => 'modalCreate',
            'size' => 'modal-md',
        ]);
        echo "<div id='modalCreateContent'></div>";
        Modal::end();

        Modal::begin([
            'title' => Yii::t('modaltitle','<?= 'Update'.Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass))) ?>'),
            'id' => 'modalUpdate',
            'size' => 'modal-md',
        ]);
        echo "<div id='modalUpdateContent'></div>";
        Modal::end();
    ?>
<?php else: ?>
    <?= "<?= " ?>ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model-><?= $nameAttribute ?>), ['view', <?= $urlParams ?>]);
        },
    ]) ?>
<?php endif; ?>
<?= $generator->enablePjax ? "    <?php Pjax::end(); ?>\n" : '' ?>
</div>
