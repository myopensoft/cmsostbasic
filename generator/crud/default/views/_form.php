<?php
// print_r($generator);
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = new $generator->modelClass();
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\touchspin\TouchSpin;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */

$create = Yii::$app->params['successCreate'];
$update = Yii::$app->params['successUpdate'];
$delete = Yii::$app->params['successDelete'];
if($model->isNewRecord) {
    $toastr = $create;
}else{
    $toastr = $update;
}
$jsBegin = <<< JS
    $('.chosen-select').chosen({width: "100%"});
    $(".select2").select2({width: "100%"});
    $("#<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form").submit(function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var form = $(this);
        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: new FormData(form[0]),
            dataType: 'json',
            contentType: false,
            processData: false,
            async: true,
            success: function(data) {
                console.log(data);
                $('#modalCreate').modal('hide');
                $('#modalUpdate').modal('hide');
                setTimeout(function() {
                    toastr.success('$toastr');
                    //$.pjax.reload({container: '#indexpjax'});
                    $.pjax.reload('#indexpjax', {timeout : false});
                }, 1000);
            },
            error: function(data) {
                var er;
                $.each(data['responseJSON'], function (key, value) {
                   er += value[0] + '\\n';
                });
                swal({
                    title: er.substr(9),
                    text: '',
                    type: 'warning',
                    confirmButtonText: 'OK',
                    confirmButtonClass: 'btn-primary btn-rounded btn-outline',
                });
            }  
        })
    });
JS;
$this->registerJs($jsBegin);

?>

<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form">

    <?= "<?php " ?>$form = ActiveForm::begin([
        'id' => '<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}{beginWrapper}{input}{hint}{error}{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 col-form-label',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

<?php foreach ($generator->getColumnNames() as $attribute) {
    if (in_array($attribute, $safeAttributes)) {
        echo "    <?= " . $generator->generateActiveField($attribute) . " ?>\n\n";
    }
} ?>
    <div class="form-group" align="center">
        <?= "<?= " ?>Html::submitButton(Yii::t('btn', 'Save'), ['class' => 'btn btn-success btn-sm']) ?>
    </div>

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
