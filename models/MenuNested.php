<?php

namespace app\models;
 
use Yii;
use kartik\tree\TreeView;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
 
class MenuNested extends \kartik\tree\models\Tree
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_menu_nested';
    }    
    
    public function rules()
    {
        /**
         * @var Module $module
         */
        $module = TreeView::module();
        $nameAttribute = $iconAttribute = $iconTypeAttribute = null;
        extract($module->dataStructure);
        $attributes = array_merge([
                        $nameAttribute, 
                        $iconAttribute,
                        $iconTypeAttribute,
                        'menu_loc',
                        'controller_name',
                        'action_name'], static::$boolAttribs);
        $rules = [
            [[$nameAttribute], 'required'],
            [$attributes, 'safe'],
        ];
        if ($this->encodeNodeNames) {
            $rules[] = [
                $nameAttribute,
                'filter',
                'filter' => function ($value) {
                    return Html::encode($value);
                },
            ];
        }
        if ($this->purifyNodeIcons) {
            $rules[] = [
                $iconAttribute,
                'filter',
                'filter' => function ($value) {
                    return HtmlPurifier::process($value);
                },
            ];
        }
        return $rules;
    }
    
    /**
     * Override isDisabled method if you need as shown in the  
     * example below. You can override similarly other methods
     * like isActive, isMovable etc.
     */
//    public function isDisabled()
//    {
//        if (Yii::$app->user->username !== 'admin') {
//            return true;
//        }
//        return parent::isDisabled();
//    }
}