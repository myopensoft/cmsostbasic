<?php

namespace app\models;

use Yii;
use yii\db\BaseActiveRecord;
use yii\db\ActiveRecord;
use yii\db\Expression;
use app\components\Cmsost;
/**
 * This is the model class for table "sys_ref".
 *
 * @property integer $id
 * @property string $cat
 * @property string $code
 * @property string $descr_ms
 * @property string $descr_en
 * @property integer $sort
 * @property string $param
 * @property string $status
 * @property string $created_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $updated_by
 */
class Ref extends Cmsost
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sys_ref';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descr_ms','status'], 'required'],
            [['sort'], 'integer'],
            [['cat', 'code'], 'string', 'max' => 25],
            [['descr_ms', 'descr_en'], 'string', 'max' => 150],
            [['param'], 'string', 'max' => 5],
            [['status'], 'string', 'max' => 1],
            [['created_by', 'updated_by'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('ref', 'ID'),
            'cat' => Yii::t('ref', 'Category'),
            'code' => Yii::t('ref', 'Code'),
            'descr_ms' => Yii::t('ref', 'Description'),
            'descr_en' => Yii::t('ref', 'Descr English'),
            'sort' => Yii::t('ref', 'Order'),
            'param' => Yii::t('ref', 'Param'),
            'status' => Yii::t('ref', 'Status'),
            'created_by' => Yii::t('ref', 'Created By'),
            'created_at' => Yii::t('ref', 'Created At'),
            'updated_at' => Yii::t('ref', 'Updated At'),
            'updated_by' => Yii::t('ref', 'Updated By'),
        ];
    }
    
    public static function GetList($Cat, $Placeholder = true, $Sorting = 'sort')
    {
        $mRef = self::find()->where(['cat'=>$Cat,'status'=>1])->orderBy($Sorting)->all();
        
        if($Placeholder)
        $arr = [''=>'-- PLEASE SELECT --'];
        else
        $arr = [];
        foreach ($mRef as $Ref) {
            $arr[$Ref->code] = $Ref->descr_en;
        }
        return $arr;
    }
    
    public static function getDesc($cat, $code, $field = '') {
        $ref = self::find()->where("cat = '$cat' AND code = '$code'")->one();
        if (!is_null($ref)) {
            if ($field)
                return $ref->$field;
            else
                return $ref->descr_en;
        } else {
            return '';
        }
    }
    
    public static function PleaseSelect()
    {
        $Ref = self::find()->where(['cat' => '46'])->one();
        $FieldName = 'descr_'.Yii::$app->language;
        $PleaseSelect = $Ref->$FieldName;
        if($PleaseSelect) {
            return $PleaseSelect;
        }else{
            return $Ref->descr_ms;
        }
    }
}
