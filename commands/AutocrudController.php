<?php
/**
 * @link http://myopensoft.net/
 * @copyright Copyright (c) 2019 Opensoft Technologies Sdn Bhd
 * @license http://myopensoft.net/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use app\models\User;

/**
 * Automatically generate CRUD for all user-defined tables in connected database
 * for use with Yii2 Advanced Template
 * @author Osh Pilaf <faizal@myopensoft.net>
 */
class AutocrudController extends Controller
{
    public $run_tables;
    public $skip_tables;

    public function options($actionID)
    {
        return array_merge(parent::options($actionID), [
            'run_tables', 'skip_tables'
        ]);
    }

    /**
     * Initial cmsost database setup
     * 
     */
    public function actionIndex()
    {
        //echo $message . "\n";
        //$msg = Console::ansiFormat($message, [Console::FG_YELLOW]);
        //$this->stdout("Use $msg to get usage info.\n");
        
        // Initial migrations 
        Yii::$app->runAction('migrate/up',['interactive'=>0]);
        // ./yii migrate --migrationPath=@yii/rbac/migrations
        Yii::$app->runAction('migrate/up',[
            'migrationPath' => '@yii/rbac/migrations',
            'interactive' => 0
        ]);
        // ./yii migrate --migrationPath=@hscstudio/mimin/migrations
        Yii::$app->runAction('migrate/up',[
            'migrationPath' => '@hscstudio/mimin/migrations',
            'interactive' => 0
        ]);
        // ./yii migrate --migrationPath=@bedezign\yii2\audit\migrations
        Yii::$app->runAction('migrate/up',[
            'migrationNamespaces' => 'bedezign\yii2\audit\migrations',
            'interactive' => 0
        ]);

        // Insert default root user
        $user = new User;
        $user->username = 'root';
        $user->full_name = 'Administrator';
        $user->auth_key = Yii::$app->security->generateRandomString();
        $user->setPassword('O$hb0t_831');
        $user->email = 'root@myopensoft.net';
        $user->status = $user::STATUS_ACTIVE;
        $user->created_at = date('Y-m-d H:i:s');
        $user->updated_at = date('Y-m-d H:i:s');
        $user->created_by = 1;
        $user->updated_by = 1;
        $user->verification_token = bin2hex(openssl_random_pseudo_bytes(64));
        if ($user->save(false)){

            $defaultUser = isset($user->id) ? $user->id : 1;
            $defaultAuth = 'Administrator';
            $defaultRoute = '/*';

            $AuthItem = new \app\models\AuthItem;
            $AuthItem->name = $defaultAuth;
            $AuthItem->type = 1;
            $AuthItem->save();

            $AuthItem = new \app\models\AuthItem;
            $AuthItem->name = $defaultRoute;
            $AuthItem->type = 2;
            $AuthItem->save();

            $AuthItemChild = new \app\models\AuthItemChild;
            $AuthItemChild->parent = $defaultAuth;
            $AuthItemChild->child = $defaultRoute;
            $AuthItemChild->save();

            $AuthAssignment = new \app\models\AuthAssignment;
            $AuthAssignment->item_name = $defaultAuth;
            $AuthAssignment->user_id = $defaultUser;
            $AuthAssignment->save();

            $this->stdout("\nUser root created successfully...\n\n");
        }

        /*
        $config = \yii\helpers\ArrayHelper::merge(
            require(Yii::getAlias('@common').'/config/main.php'),
            require(Yii::getAlias('@common').'/config/main-local.php'),
            require(Yii::getAlias('@backend').'/config/main.php'),
            require(Yii::getAlias('@backend').'/config/main-local.php')
        );

        $web_application = new \yii\web\Application($config);
        $web_application->runAction('/mimin/route/generate');
        */
        
        $msg = Console::ansiFormat('completed', [Console::FG_YELLOW]);
        $this->stdout("Autocrud init $msg successfully.\n");

    }
    
    /**
     * Create complete model + CRUD for all user-defined tables in schema
     * Option 1: run_tables = list of tables to run seperated by comma i.e: --run_tables=table1,table2
     * Option 2: skip_tables = list of tables to skip seperated by comma i.e: --skip_tables=table3,table4
     * ./yii autocrud/rollout --run_tables=table1,table2 --skip_tables=table3,table4
     */
    public function actionRollout($modelNamespace='backend', $crudNamespace='backend')
    {
        /*
         * gii/model
         *
         * --enableI18N=1
         * --modelClass=Demo (no namespace)
         * --ns=common\\models
         * --tableName=demo
         * --generateRelations=all (options: none, all, all-inverse)
         * --interactive=0
         * --overwrite=1
         *
         * gii/crud
         *
         * --controllerClass=backend\\controllers\\DemoController (fully qualified namespace)
         * --viewPath=@backend/views/demo
         * --enableI18N=1
         * --enablePjax=1
         * --indexWidgetType=grid (options: grid, list)
         * --interactive=0
         * --modelClass=common\\models\\Demo
         * --overwrite=1
         * --searchModelClass=common\\models\\DemoSearch
         * 
         */
        
        //$run_tables = [];
        $this->skip_tables = [];
        $this->run_tables = [];
        if (isset($this->skip_tables) && $this->skip_tables !== null){
            $this->skip_tables = explode(',',$this->skip_tables);
        }
        if (isset($this->run_tables) && $this->run_tables !== null){
            $this->run_tables = explode(',',$this->run_tables);
        }
 
        // Skip CMS default tables
        $skip_tables_default = [
            'auth_assignment',
            'auth_item',
            'auth_item_child',
            'auth_rule',
            'message',
            'migration',
            'route',
            'source_message',
            'sys_menu',
            'sys_menu_nested',
            'sys_ref',
            'user',
        ];

        $skip_tables = array_merge($skip_tables_default, $this->skip_tables);

        // Get all user defined tables
        $db_connection = \Yii::$app->db;
        $dbSchema = $db_connection->schema;
        $tables = $dbSchema->getTableNames();
        foreach($tables as $tbl){
            if (!in_array($tbl, $skip_tables)){
                $run_tables[] = $tbl;
            }
        }
        
        $run_tables = array_merge($run_tables, $this->run_tables);

        if (is_array($run_tables) && !empty($run_tables)){
            $modelNamespace = "$modelNamespace\\models";
            foreach ($run_tables as $rt){
                // Generate Model
                $modelName = $this->underscoreToCamelCase($rt, 1);
                Yii::$app->runAction('gii/model',[
                    'enableI18N' => 1,
                    'modelClass' => $modelName,
                    'ns' => $modelNamespace,
                    'tableName' => $rt,
                    'generateRelations' => 'all',
                    'interactive' => 0,
                    'overwrite' => 1
                ]);
                // Generate CRUD files
                $controllerName = $modelName.'Controller';
                $modelSearch = $modelName.'Search';
                Yii::$app->runAction('gii/crud',[
                    'controllerClass' => "$crudNamespace\\controllers\\$controllerName",
                    'viewPath' => "$crudNamespace/views/".strtolower($modelName),
                    'enableI18N' => 1,
                    'enablePjax' => 1,
                    'indexWidgetType' => 'grid',
                    'interactive' => 0,
                    'modelClass' => "$modelNamespace\\$modelName",
                    'overwrite' => 1,
                    'searchModelClass' => "$modelNamespace\\$modelSearch"
                ]);
            }
        }
        
        $message = 'Models & CRUDs generation successfull!';
        $message = Console::ansiFormat($message, [Console::FG_YELLOW]);
        $this->stdout("$message\n"); 
    }
    
    private function underscoreToCamelCase( $string, $first_char_caps = false)
    {
        if( $first_char_caps == true )
        {
            $string[0] = strtoupper($string[0]);
        }
        // $func = create_function('$c', 'return strtoupper($c[1]);');
        $func = function($c) { return strtoupper($c[1]); };
        return preg_replace_callback('/_([a-z])/', $func, $string);
    }
}