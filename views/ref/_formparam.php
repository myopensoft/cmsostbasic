<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\widgets\Pjax;
use kartik\widgets\SwitchInput;
use app\models\Ref;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Ref */
/* @var $form yii\widgets\ActiveForm */
$create = Yii::$app->params['successCreate'];
$update = Yii::$app->params['successUpdate'];
if($model->isNewRecord) {
    $toastr = $create;
}else{
    $toastr = $update;
}
$urlRedirect = Url::to(['index-param','id'=>'']);
$jsBegin = <<< JS
$("#form-param").submit(function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    var form = $(this);
    $.ajax({
        url: form.attr('action'),
        method: form.attr('method'),
        data: new FormData(form[0]),
        dataType: 'json',
        contentType: false,
        processData: false,
        async: true,
        success: function(data) {
            $('#modalCreateParam').modal('hide');
            $('#modalUpdateParam').modal('hide');
            toastr.success('$toastr');
            $.pjax({url: '$urlRedirect' + data.id, container: '#indexparampjax', push: false});
        },
        error: function(data) {
            var er;
            $.each(data['responseJSON'], function (key, value) {
                er += value[0] + '\\n';
            });
            swal({
                title: er.substr(9),
                text: '',
                type: 'warning',
                confirmButtonText: 'OK',
                confirmButtonClass: 'btn-primary btn-rounded btn-outline',
            });
        }
    })
});
JS;
$this->registerJs($jsBegin);
?>
<?php Pjax::begin(['id'=>'form-param-pjax','timeout'=>false,'enablePushState'=>false]); ?>
<div class="form-param">

    <?php $form = ActiveForm::begin([
        'id' => 'form-param',
        'layout' => 'horizontal',
        'options' => ['data-pjax' => true ],
        'fieldConfig' => [
            'template' => "{label}{beginWrapper}{input}{hint}{error}{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 col-form-label',
                //'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-9 input-group',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'code')->textInput(['class' => 'form-control input-sm', 'maxlength' => true]) ?>

    <?= $form->field($model, 'descr_ms')->textInput(['class' => 'form-control input-sm', 'maxlength' => true]) ?>

    <?= $form->field($model, 'descr_en')->textInput(['class' => 'form-control input-sm', 'maxlength' => true]) ?>

    <?= $form->field($model, 'sort')->textInput(['class' => 'form-control input-sm']) ?>

    <?= $form->field($model, 'status', ['inputOptions' => ['id' => $model->isNewRecord? 'status_create':'status_update']])->widget(SwitchInput::classname(), [
        'containerOptions' => [], 
        'pluginOptions' => [
                'size' => 'small',
                'handleWidth'=>80,
                'onText' => Ref::getDesc(39,1),
                'offText' => Ref::getDesc(39,0),
                'onColor' => 'primary',
                'offColor' => 'danger',
        ]]);
    ?>

    <div class="form-group" align="center">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Simpan') : Yii::t('app', 'Kemaskini'), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php Pjax::end(); ?>