<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Ref */

$this->title = Yii::t('app', 'Kemaskini {modelClass}: ', [
    'modelClass' => 'Parameter',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pentadbiran')];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Selenggara Parameter'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Kemaskini');
?>
<div class="ref-update">

    <!--<h1><?php //= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
