<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\RefSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('breadcrumbs_ref', 'Manage Parameter');
$this->params['breadcrumbs'][] = ['label' => Yii::t('breadcrumbs_ref', 'Administrator')];
$this->params['breadcrumbs'][] = $this->title;

Pjax::begin(['id'=>'jquerypjax','enablePushState' => false]);
$urlCreate = Url::to(['create']);
$urlUpdate = Url::to(['update','id'=>'']);
$urlListParam = Url::to(['index-param','id'=>'']);
$urlCreateParam = Url::to(['create-param','cat'=>'']);
$urlUpdateParam = Url::to(['update-param','id'=>'']);

$jsBegin = <<< JS
    function create() {
        $('#modalCreate').modal('show').find('#modalCreateContent').load('$urlCreate');
        return false;
    }
    function update(id) {
        $('#modalUpdate').modal('show').find('#modalUpdateContent').load('$urlUpdate' + id);
        return false;
    }
    function listparam(id) {
        $('#modalListParam').modal('show').find('#modalListParamContent').load('$urlListParam' + id);
        return false;
    }
    function createparam(cat) {
        $('#modalCreateParam').modal('show').find('#modalCreateParamContent').load('$urlCreateParam' + cat);
        return false;
    }
    function updateparam(id) {
        $('#modalUpdateParam').modal('show').find('#modalUpdateParamContent').load('$urlUpdateParam' + id);
        return false;
    }
JS;
$this->registerJs($jsBegin, yii\web\View::POS_BEGIN);
Pjax::end();
?>

<div class="ref-index tooltip-demo">

    <?php Pjax::begin(['id'=>'indexcatpjax','timeout'=>false,'enablePushState'=>false]); ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-search"></i> <?= Yii::t('titlepanel', 'Search') ?>
        </div>
        <div class="panel-body">
            <?= $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
    
    <?php
        $gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
            'code',
            'descr_ms',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {index-param} {delete}',
                'header' => 'Action',
                'contentOptions'=>['style'=>'width: 10%;'],
                'buttons' => [
                    'update' => function ($url,$model) {
                        return Html::button('<i class="fa fa-pencil-alt"></i>', [
                            'class' => 'btn btn-success btn-xs btn-rounded btn-outline',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => Yii::t('tooltip', 'Update'),
                            'onClick'=>"update($model->id)",
                        ]);
                    },
                    'index-param' => function ($url,$model) {
                        return Html::button('<i class="fa fa-bars"></i>', [
                            'class' => 'btn btn-info btn-xs btn-rounded btn-outline',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => Yii::t('tooltip', 'List'),
                            'onClick'=>"listparam($model->id)",
                        ]);
                    },
                    'delete' => function ($url) {
                        $sweetdeleteconfirmtitle = Yii::t('sweetdelete', 'titleconfirm');
                        $sweetdeleteconfirmtext = Yii::t('sweetdelete', 'textconfirm');
                        $sweetdeletesuccesstitle = Yii::t('sweetdelete', 'titlesuccess');
                        $sweetdeletesuccesstext = Yii::t('sweetdelete', 'textsuccess');
                        return Html::button('<i class="fa fa-trash"></i>', [
                            'class' => 'btn btn-danger btn-xs btn-rounded btn-outline',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => Yii::t('tooltip', 'Delete'),
                            'onclick' => "
                                swal({
                                    title: '$sweetdeleteconfirmtitle',
                                    text: '$sweetdeleteconfirmtext',
                                    type: 'warning',
                                    showCancelButton: true,
                                    cancelButtonText: 'No',
                                    cancelButtonClass: 'btn-primary btn-rounded btn-outline',
                                    confirmButtonText: 'Yes',
                                    confirmButtonClass: 'btn-danger btn-rounded btn-outline',
                                    closeOnConfirm: false
                                }, function () {
                                    $.ajax('$url', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        $.pjax.reload({container: '#indexcatpjax'});
                                        swal({
                                        title: '$sweetdeletesuccesstitle',
                                        text: '$sweetdeletesuccesstext',
                                        type: 'success',
                                        confirmButtonClass: 'btn-primary btn-rounded btn-outline',
                                        });
                                    });
                                });
                            ",
                        ]);
                    },
                ]
            ],
        ];
                    
        $fullExportMenu =  
        Html::button('<i class="fa fa-plus"></i> '.Yii::t('btn_create_ref', 'Category'), ['class' => 'btn btn-success btn-sm btn-rounded btn-outline', 'onClick'=>"create()",]).
        ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_BLANK,
            'fontAwesome' => true,
            'columnSelectorOptions'=>[
                'label' => 'Columns',
                'class' => 'btn btn-default btn-sm btn-rounded btn-outline'
            ],
            'dropdownOptions' => [
                'label' => 'Full',
                'class' => 'btn btn-default btn-sm btn-rounded btn-outline',
                'itemsBefore' => [
                    '<li class="dropdown-header">Export All Data</li>',
                ],
            ],
            'pjaxContainerId' => 'kv-pjax-container',
        ]);
    ?>
    
    <?= GridView::widget([
        'id' => 'grid-cat',
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'toolbarContainerOptions' => ['class' => 'btn-toolbar kv-grid-toolbar toolbar-container float-left'],
        'pjax' => true,
        'panel' => [
            // 'type' => GridView::TYPE_PRIMARY,
            'heading' => '<i class="fa fa-bars"></i> '.Yii::t('titlepanel', 'List'),
        ],
        // set a label for default menu
        'export' => [
            'label' => 'Page',
            'fontAwesome' => true,
            'options' => ['class' => 'btn btn-default btn-sm btn-rounded btn-outline'],
        ],
        'toolbar' => [
            $fullExportMenu,
            '{export}',
        ],
        'responsive' => true,
        'responsiveWrap' => false,
    ]); ?>
    <?php
        Modal::begin([
            'title' => Yii::t('modaltitle_ref', 'CreateCat'),
            'id' => 'modalCreate',
            'size' => 'modal-md',
        ]);
        echo "<div id='modalCreateContent'></div>";
        Modal::end();
        Modal::begin([
            'title' => Yii::t('modaltitle_ref', 'UpdateCat'),
            'id' => 'modalUpdate',
            'size' => 'modal-md',
        ]);
        echo "<div id='modalUpdateContent'></div>";
        Modal::end();
        Modal::begin([
            'title' => Yii::t('modaltitle_ref', 'ListParam'),
            'id' => 'modalListParam',
            'size' => 'modal-lg',
            'options' => ['style' => 'overflow:auto !important;']
        ]);
        echo "<div id='modalListParamContent'></div>";
        Modal::end();
        Modal::begin([
            'title' => Yii::t('modaltitle_ref', 'CreateParam'),
            'id' => 'modalCreateParam',
            'size' => 'modal-md',
        ]);
        echo "<div id='modalCreateParamContent'></div>";
        Modal::end();
        Modal::begin([
            'title' => Yii::t('modaltitle_ref', 'UpdateParam'),
            'id' => 'modalUpdateParam',
            'size' => 'modal-md',
        ]);
        echo "<div id='modalUpdateParamContent'></div>";
        Modal::end();
    ?>
    <?php Pjax::end(); ?>
</div>