<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Ref */

$this->title = Yii::t('app', 'Create Ref');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Refs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-create">

    <!--<h1><?php //= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
