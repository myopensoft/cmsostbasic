<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Ref */

$this->title = Yii::t('app', 'Create Parameter');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Refs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-create">

    <!--<h1><?php //= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_formparam', [
        'model' => $model,
    ]) ?>

</div>
