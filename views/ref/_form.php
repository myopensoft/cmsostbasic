<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Ref */
/* @var $form yii\widgets\ActiveForm */
$create = Yii::$app->params['successCreate'];
$update = Yii::$app->params['successUpdate'];
if($model->isNewRecord) {
    $toastr = $create;
}else{
    $toastr = $update;
}
$jsBegin = <<< JS
$("#form-index-cat").submit(function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    var form = $(this);
    $.ajax({
        url: form.attr('action'),
        method: form.attr('method'),
        data: new FormData(form[0]),
        dataType: 'json',
        contentType: false,
        processData: false,
        async: true,
        success: function(data) {
            $('#modalCreate').modal('hide');
            $('#modalUpdate').modal('hide');
            setTimeout(function() {
                toastr.success('$toastr');
                $.pjax.reload({container: '#indexcatpjax'});
            }, 1000);
        },
        error: function(data) {
            var er;
            $.each(data['responseJSON'], function (key, value) {
                er += value[0] + '\\n';
            });
            swal({
                title: er.substr(9),
                text: '',
                type: 'warning',
                confirmButtonText: 'OK',
                confirmButtonClass: 'btn-primary btn-rounded btn-outline',
            });
        }
    })
});
JS;
$this->registerJs($jsBegin);
?>

<div class="ref-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-index-cat',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}{beginWrapper}{input}{hint}{error}{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 col-form-label',
                //'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-9 input-group',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'descr_ms')->textInput(['class' => 'form-control input-sm', 'maxlength' => true])->label('Category') ?>

    <div class="form-group" align="center">
        <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-save']).' '.Yii::t('btn', 'save') : Html::tag('i', '', ['class' => 'fa fa-save']).' '.Yii::t('btn', 'update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm btn-rounded btn-outline' : 'btn btn-primary btn-sm btn-rounded btn-outline']) ?>
        <?php //= Html::submitButton($model->isNewRecord ? Yii::t('btn', 'Create') : Yii::t('btn', 'Save'), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
        <?php //= Html::button(Yii::t('btn', 'Close'), ['class' => 'btn btn-default btn-sm', 'onClick'=>"closeModal()",]); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
