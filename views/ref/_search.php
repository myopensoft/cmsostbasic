<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\RefSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-search">

<?php $form = ActiveForm::begin([
        'id' => 'cat-search',
        'layout' => 'horizontal',
        'action' => ['index-cat'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
        'fieldConfig' => [
            'template' => "{label}{beginWrapper}{input}{hint}{error}{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 col-form-label',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'code')->textInput(['class' => 'form-control input-sm']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'descr_ms')->textInput(['class' => 'form-control input-sm']) ?>
        </div>
    </div>
    

    

    <div class="form-group" align="center">
        <?= Html::submitButton(Html::tag('i', '', ['class' => 'fa fa-search']).' '.Yii::t('btn', 'Search'), ['id' => 'search-btn', 'class' => 'btn btn-sm btn-primary btn-rounded btn-outline']); ?>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-sync-alt']).' '.Yii::t('btn', 'Reset'), [''],['class' => 'btn btn-warning btn-sm btn-rounded btn-outline']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
