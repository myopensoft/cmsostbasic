<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Message */
/* @var $form yii\widgets\ActiveForm */
$create = Yii::$app->params['successCreate'];
$update = Yii::$app->params['successUpdate'];
if($model->isNewRecord) {
    $toastr = $create;
}else{
    $toastr = $update;
}
$urlRedirect = Url::to(['message','id'=>'']);
$jsBegin = <<< JS
$("#message-create-form").submit(function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    var form = $(this);
    $.ajax({
        url: form.attr('action'),
        method: form.attr('method'),
        data: new FormData(form[0]),
        dataType: 'json',
        contentType: false,
        processData: false,
        async: true,
        success: function(data) {
            $('#modalCreateTranslationLanguage').modal('hide');
            $('#modalUpdateTranslationLanguage').modal('hide');
            toastr.success('$toastr');
            $.pjax({url: '$urlRedirect' + data.id, container: '#indexmessage', push: false});
            return false;
        },
        error: function(data) {
            var er;
            $.each(data['responseJSON'], function (key, value) {
                er += value[0] + '\\n';
            });
            swal({
                title: er.substr(9),
                text: '',
                type: 'warning',
                confirmButtonText: 'OK',
                confirmButtonClass: 'btn-primary btn-rounded btn-outline',
            });
        }
    })
});
JS;
$this->registerJs($jsBegin);
?>

<div class="message-form">

    <?php $form = ActiveForm::begin([
    	'id' => 'message-create-form',
    	'layout' => 'horizontal',
    	'fieldConfig' => [
            'template' => "{label}{beginWrapper}{input}{hint}{error}{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 col-form-label',
                //'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-9 input-group',
                'error' => '',
                'hint' => '',
            ],
        ],
	]); ?>

    <?= $form->field($model, 'language')->textInput(['class' => 'form-control input-sm', 'maxlength' => true]) ?>

    <?= $form->field($model, 'translation')->textarea(['class' => 'form-control input-sm']) ?>

    <div class="form-group" align="center">
        <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-save']).' '.Yii::t('btn', 'save') : Html::tag('i', '', ['class' => 'fa fa-save']).' '.Yii::t('btn', 'update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm btn-rounded btn-outline' : 'btn btn-primary btn-sm btn-rounded btn-outline']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
