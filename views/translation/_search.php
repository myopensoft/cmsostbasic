<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\models\SourceMessage;

/* @var $this yii\web\View */
/* @var $model app\models\SourceMessageSearch */
/* @var $form yii\widgets\ActiveForm */
$jsReady = <<< JS
    $(".select2").select2();
JS;
$this->registerJs($jsReady, yii\web\View::POS_READY);
?>

<div class="source-message-search">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
        'fieldConfig' => [
            'template' => "{label}{beginWrapper}{input}{hint}{error}{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4 col-form-label',
                //'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class='row'>
        <div class="col-sm-6">
            <?= $form->field($model, 'category')->dropDownlist(SourceMessage::ListCategory(),['class' => 'form-control input-sm select2']) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'message')->textInput(['class' => 'form-control input-sm']) ?>
        </div>
    </div>

    <div class="form-group" align="center">
        <?= Html::submitButton(Html::tag('i', '', ['class' => 'fa fa-search']).' '.Yii::t('btn', 'Search'), ['id' => 'search-btn', 'class' => 'btn btn-sm btn-primary btn-rounded btn-outline']); ?>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-sync-alt']).' '.Yii::t('btn', 'Reset'), ['index'],['class' => 'btn btn-warning btn-sm btn-rounded btn-outline', 'data-pjax' => 0]) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
