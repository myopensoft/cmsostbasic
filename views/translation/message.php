<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="message-index">

    <?php Pjax::begin(['id'=>'indexmessage','enablePushState'=>false,'timeout'=>false]); ?>
    
    <?php
        $gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
            'language',
            'translation:ntext',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete-message}',
                'header' => 'Action',
                'contentOptions'=>['style'=>'width: 10%;'],
                'buttons' => [
                    'update' => function ($url,$model) {
                        return Html::button('<i class="fa fa-pencil-alt"></i>', [
                            'class' => 'btn btn-success btn-xs btn-rounded btn-outline',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => Yii::t('tooltip', 'Update'),
                            'onClick'=>"updatetranslationlanguage($model->id,'$model->language')",
                        ]);
                    },
                    'delete-message' => function ($url,$model){
                        $sweetdeleteconfirmtitle = Yii::t('sweetdelete', 'titleconfirm');
                        $sweetdeleteconfirmtext = Yii::t('sweetdelete', 'textconfirm');
                        $sweetdeletesuccesstitle = Yii::t('sweetdelete', 'titlesuccess');
                        $sweetdeletesuccesstext = Yii::t('sweetdelete', 'textsuccess');
                        $urlRedirect = Url::to(['message', 'id'=>'']);
                        return Html::button('<i class="fa fa-trash"></i>', [
                            'class' => 'btn btn-danger btn-xs btn-rounded btn-outline',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'top',
                            'title' => Yii::t('tooltip', 'Delete'),
                            'onclick' => "
                                swal({
                                    title: '$sweetdeleteconfirmtitle',
                                    text: '$sweetdeleteconfirmtext',
                                    type: 'warning',
                                    showCancelButton: true,
                                    cancelButtonText: 'No',
                                    cancelButtonClass: 'btn-primary btn-rounded btn-outline',
                                    confirmButtonText: 'Yes',
                                    confirmButtonClass: 'btn-danger btn-rounded btn-outline',
                                    closeOnConfirm: false
                                }, function () {
                                    $.ajax('$url' + '&id=$model->id&language=$model->language', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        $.pjax({url: '$urlRedirect' + data.id, container: '#indexmessage', push: false});
                                        swal({  
                                        title: '$sweetdeletesuccesstitle',
                                        text: '$sweetdeletesuccesstext',
                                        type: 'success',
                                        confirmButtonClass: 'btn-primary btn-rounded btn-outline',
                                        });
                                    });
                                });
                            ",
                        ]);
                    },
                ]
            ],
        ];
                    
        $fullExportMenu =  
        Html::button('<i class="fa fa-plus"></i> '.Yii::t('btncreatemessage', 'Translation'), ['class' => 'btn btn-success btn-sm btn-rounded btn-outline', 'onClick'=>"createtranslationlanguage($id)"]).
        ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_BLANK,
            'fontAwesome' => true,
            'columnSelectorOptions'=>[
                'label' => 'Columns',
                'class' => 'btn btn-default btn-sm btn-rounded btn-outline'
            ],
            'dropdownOptions' => [
                'label' => 'Full',
                'class' => 'btn btn-default btn-sm btn-rounded btn-outline',
                'itemsBefore' => [
                    '<li class="dropdown-header">Export All Data</li>',
                ],
            ],
            'pjaxContainerId' => 'kv-pjax-container',
        ]);
    ?>
    
    <?= GridView::widget([
        'id' => 'grid-cat',
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'toolbarContainerOptions' => ['class' => 'btn-toolbar kv-grid-toolbar toolbar-container float-left'],
        'pjax' => true,
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'heading' => '<i class="fa fa-bars"></i> '.Yii::t('titlepanel', 'List'),
        ],
        // set a label for default menu
        'export' => [
            'label' => 'Page',
            'fontAwesome' => true,
            'options' => ['class' => 'btn btn-default btn-sm btn-rounded btn-outline'],
        ],
        'toolbar' => [
            $fullExportMenu,
            '{export}',
        ],
        'responsive' => true,
        'responsiveWrap' => false,
    ]); ?>

    <?php Pjax::end(); ?>
</div>