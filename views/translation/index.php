<?php 

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\SourceMessageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('breadcrumbs_sourcemessage', 'Manage Translation');
$this->params['breadcrumbs'][] = $this->title;

$urlCreate = Url::to(['create']);
$urlUpdate = Url::to(['update','id'=>'']);
$urlListTranslationLanguage = Url::to(['message','id'=>'']);
$urlCreateTranslationLanguage = Url::to(['create-message','id'=>'']);
$urlUpdateTranslationLanguage = Url::to(['update-message','id'=>'']);
$jsBegin = <<< JS
    function create() {
        $('#modalCreate').modal('show').find('#modalCreateContent').load('$urlCreate');
        return false;
    }
    function update(id) {
        $('#modalUpdate').modal('show').find('#modalUpdateContent').load('$urlUpdate' + id);
        return false;
    }
    function listtranslationlanguage(id) {
        $('#modalListTranslationLanguage').modal('show').find('#modalListTranslationLanguageContent').load('$urlListTranslationLanguage' + id);
        return false;
    }
    function createtranslationlanguage(id) {
        $('#modalCreateTranslationLanguage').modal('show').find('#modalCreateTranslationLanguageContent').load('$urlCreateTranslationLanguage' + id);
        return false;
    }
    function updatetranslationlanguage(id,language) {
        $('#modalUpdateTranslationLanguage').modal('show').find('#modalUpdateTranslationLanguageContent').load('$urlUpdateTranslationLanguage' + id + '&language=' + language);
        return false;
    }
JS;
$this->registerJs($jsBegin, yii\web\View::POS_END);

?>
<div class="source-message-index tooltip-demo">
    
    <?php Pjax::begin(['id' => 'indexsourcemessage','timeout' => false,'enablePushState' => false]); ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-search"></i> <?= Yii::t('titlepanel', 'Search') ?>
        </div>
        <div class="panel-body">
            <?= $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>

    <?php
        $gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],
            'category',
            'message:ntext',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {message} {delete}',
                'header' => 'Action',
                'contentOptions'=>['style'=>'width: 10%;'],
                'buttons' => [
                    'update' => function ($url,$model) {
                        return Html::button('<i class="fa fa-pencil-alt"></i>', [
                            'class' => 'btn btn-success btn-xs btn-rounded btn-outline',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'bottom',
                            'title' => Yii::t('tooltip', 'Update'),
                            'onClick'=>"update($model->id)",
                        ]);
                    },
                    'message' => function ($url,$model) {
                        return Html::button('<i class="fa fa-bars"></i>', [
                            'class' => 'btn btn-primary btn-xs btn-rounded btn-outline',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'bottom',
                            'title' => Yii::t('tooltip', 'Message'),
                            'onclick' => "listtranslationlanguage($model->id)"
                        ]);
                    },
                    'delete' => function ($url){
                        $sweetdeleteconfirmtitle = Yii::t('sweetdelete', 'titleconfirm');
                        $sweetdeleteconfirmtext = Yii::t('sweetdelete', 'textconfirm');
                        $sweetdeletesuccesstitle = Yii::t('sweetdelete', 'titlesuccess');
                        $sweetdeletesuccesstext = Yii::t('sweetdelete', 'textsuccess');
                        return Html::button('<i class="fa fa-trash"></i>', [
                            'class' => 'btn btn-danger btn-xs btn-rounded btn-outline',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'bottom',
                            'title' => Yii::t('tooltip', 'Delete'),
                            'onclick' => "
                                swal({
                                    title: '$sweetdeleteconfirmtitle',
                                    text: '$sweetdeleteconfirmtext',
                                    type: 'warning',
                                    showCancelButton: true,
                                    cancelButtonText: 'No',
                                    cancelButtonClass: 'btn-primary btn-rounded btn-outline',
                                    confirmButtonText: 'Yes',
                                    confirmButtonClass: 'btn-danger btn-rounded btn-outline',
                                    closeOnConfirm: false
                                }, function () {
                                    $.ajax('$url', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        $.pjax.reload({container: '#indexsourcemessage'});
                                        swal({
                                        title: '$sweetdeletesuccesstitle',
                                        text: '$sweetdeletesuccesstext',
                                        type: 'success',
                                        confirmButtonClass: 'btn-primary btn-rounded btn-outline',
                                        });
                                    });
                                });
                            ",
                        ]);
                    },
                ]
            ],
        ];
                    
        $fullExportMenu =  
        Html::button('<i class="fa fa-plus"></i> '.Yii::t('btncreatesourcemessage', 'Translation Tag'), ['class' => 'btn btn-success btn-sm btn-rounded btn-outline', 'onClick'=>"create()"]).
        ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_BLANK,
            'fontAwesome' => true,
            'columnSelectorOptions'=>[
                'label' => 'Columns',
                'class' => 'btn btn-default btn-sm btn-rounded btn-outline'
            ],
            'dropdownOptions' => [
                'label' => 'Full',
                'class' => 'btn btn-default btn-sm btn-rounded btn-outline',
                'itemsBefore' => [
                    '<li class="dropdown-header">Export All Data</li>',
                ],
            ],
            'pjaxContainerId' => 'kv-pjax-container',
        ]);
    ?>

    <?= GridView::widget([
        'id' => 'grid-cat',
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'toolbarContainerOptions' => ['class' => 'btn-toolbar kv-grid-toolbar toolbar-container float-left'],
        'pjax' => true,
        'panel' => [
            // 'type' => GridView::TYPE_SUCCESS,
            'heading' => '<i class="fa fa-bars"></i> '.Yii::t('titlepanel', 'List'),
        ],
        // set a label for default menu
        'export' => [
            'label' => 'Page',
            'fontAwesome' => true,
            'options' => ['class' => 'btn btn-default btn-sm btn-rounded btn-outline'],
        ],
        'toolbar' => [
            $fullExportMenu,
            '{export}',
        ],
        'responsive' => true,
        'responsiveWrap' => false,
    ]); ?>

    <?php
        Modal::begin([
            'title' => Yii::t('modaltitlesourcemessage', 'Create Translation Tag'),
            'id' => 'modalCreate',
            'size' => 'modal-md',
        ]);
        echo "<div id='modalCreateContent'></div>";
        Modal::end();
        Modal::begin([
            'title' => Yii::t('modaltitlesourcemessage', 'Update Translation Tag'),
            'id' => 'modalUpdate',
            'size' => 'modal-md',
        ]);
        echo "<div id='modalUpdateContent'></div>";
        Modal::end();
        Modal::begin([
            'title' => Yii::t('modaltitlemessage', 'List Translation Language'),
            'id' => 'modalListTranslationLanguage',
            'size' => 'modal-lg',
            'options' => ['style' => 'overflow:auto !important;']
        ]);
        echo "<div id='modalListTranslationLanguageContent'></div>";
        Modal::end();
        Modal::begin([
            'title' => Yii::t('modaltitlemessage', 'Create Translation Language'),
            'id' => 'modalCreateTranslationLanguage',
            'size' => 'modal-md',
        ]);
        echo "<div id='modalCreateTranslationLanguageContent'></div>";
        Modal::end();
        Modal::begin([
            'title' => Yii::t('modaltitlemessage', 'Update Translation Language'),
            'id' => 'modalUpdateTranslationLanguage',
            'size' => 'modal-md',
        ]);
        echo "<div id='modalUpdateTranslationLanguageContent'></div>";
        Modal::end();
    ?>
    
    <?php Pjax::end(); ?>

</div>