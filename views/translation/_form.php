<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\models\SourceMessage;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
$create = Yii::$app->params['successCreate'];
$update = Yii::$app->params['successUpdate'];
if($model->isNewRecord) {
    $toastr = $create;
}else{
    $toastr = $update;
}
$jsBegin = <<< JS
    $("form").submit(function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var form = $(this);
        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: new FormData(form[0]),
            dataType: 'json',
            contentType: false,
            processData: false,
            async: true,
            success: function(data) {
                $('#modalCreate').modal('hide');
                $('#modalUpdate').modal('hide');
                setTimeout(function() {
                    toastr.success('$toastr');
                    $.pjax.reload({container: '#indexsourcemessage'});
                }, 1000);
            },
            error: function(data) {
                console.log(data);
                var er;
                $.each(data['responseJSON'], function (key, value) {
                    er += value[0] + '\\n';
                });
                swal({
                    title: er.substr(9),
                    text: '',
                    type: 'warning',
                    confirmButtonText: 'OK',
                    confirmButtonClass: 'btn-primary btn-rounded btn-outline',
                });
            }
        })
    });
JS;
$jsReady = <<< JS
    $(".select2").select2({
        width: '100%'
    });
JS;
$this->registerJs($jsReady, yii\web\View::POS_READY);
$this->registerJs($jsBegin);

?>

<div class="source-message-form">

    <?php $form = ActiveForm::begin([
        'id' => 'source-message-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}{beginWrapper}{input}{hint}{error}{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 col-form-label',
                //'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'category')->textInput(['class' => 'form-control input-sm']) ?>

    <?= $form->field($model, 'message')->textarea(['class' => 'form-control input-sm', ]) ?>

    <div class="form-group" align="center">
        <?= Html::submitButton($model->isNewRecord ? Html::tag('i', '', ['class' => 'fa fa-save']).' '.Yii::t('btn', 'save') : Html::tag('i', '', ['class' => 'fa fa-save']).' '.Yii::t('btn', 'update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm btn-rounded btn-outline' : 'btn btn-primary btn-sm btn-rounded btn-outline']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>