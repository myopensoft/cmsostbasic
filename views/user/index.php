<?php

use yii\helpers\Html;
use kartik\export\ExportMenu;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\modules\admin\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('breadcrumbs_user', 'Manage User');
$this->params['breadcrumbs'][] = $this->title;

$urlCreate = Url::to(['create']);
$urlUpdate = Url::to(['update','id'=>'']);
$urlView = Url::to(['view','id'=>'']);

$jsBegin = <<< JS
    function create() {
        $('#modalCreate').modal('show').find('#modalCreateContent').load('$urlCreate');
        return false;
    }
    function update(id) {
        $('#modalUpdate').modal('show').find('#modalUpdateContent').load('$urlUpdate' + id);
        return false;
    }
    function view(id) {
        $('#modalView').modal('show').find('#modalViewContent').load('$urlView' + id);
        return false;
    }
    
JS;
$this->registerJs($jsBegin, yii\web\View::POS_BEGIN);

?>
<div class="user-index tooltip-demo">

    <?php Pjax::begin(['id' => 'indexpjax','timeout' => false,'enablePushState' => false]); ?>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-search"></i> <?= Yii::t('titlepanel', 'Search') ?>
        </div>
        <div class="panel-body">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>

    <?php
        $gridColumns = [
            ['class' => 'yii\grid\SerialColumn'],

            'username',
            [
                'attribute' => 'role',
                'format' => 'raw',
                'value' => function($model){
                    return $model->ShowRole();
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'options' => [
                    'width' => '80px',
                ],
                'value' => function ($model) {
                    return Html::tag('span', Yii::$app->language == 'ms'? $model->statusDescr->descr:$model->statusDescr->descr_en, ['class' => $model->status == 1? 'label label-primary':'label label-danger']);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {view}',
                'header' => 'Action',
                'contentOptions'=>['style'=>'width: 10%;'],
                'buttons' => [
                   'update' => function ($url,$model) {
                       return Html::button('<i class="fa fa-pencil-alt"></i>', [
                           'class' => 'btn btn-success btn-xs btn-rounded btn-outline',
                           'data-toggle' => 'tooltip',
                           'data-placement' => 'bottom',
                           'title' => Yii::t('tooltip', 'Update'),
                           'onClick'=>"update($model->id)",
                       ]);
                   },
                   'view' => function ($url,$model) {
                        return Html::button('<i class="fa fa-eye"></i>', [
                            'class' => 'btn btn-success btn-xs btn-rounded btn-outline',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'bottom',
                            'title' => Yii::t('tooltip', 'Role'),
                            'onClick'=>"view($model->id)",
                        ]);
                    },
                   'delete' => function ($url) {
                        $sweetdeletetitle = Yii::t('sweetdelete', 'title');
                        $sweetdeletetitle1 = Yii::t('sweetdelete', 'title1');
                        $sweetdeletetext = Yii::t('sweetdelete', 'text');
                        $sweetdeletetext1 = Yii::t('sweetdelete', 'text1');
                        $delete = Yii::$app->params['successDelete'];
                        return Html::button('<i class="fa fa-trash"></i>', [
                            'class' => 'btn btn-danger btn-xs btn-rounded btn-outline',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'bottom',
                            'title' => Yii::t('tooltip', 'Delete'),
                            'onclick' => "
                                swal({
                                    title: '$sweetdeletetitle',
                                    text: '$sweetdeletetext',
                                    type: 'warning',
                                    showCancelButton: true,
                                    cancelButtonText: 'No',
                                    cancelButtonClass: 'btn-primary btn-rounded btn-outline',
                                    confirmButtonText: 'Yes',
                                    confirmButtonClass: 'btn-danger btn-rounded btn-outline',
                                    closeOnConfirm: false
                                }, function () {
                                    $.ajax('$url', {
                                        type: 'POST'
                                    }).done(function(data) {
                                        $.pjax.reload({container: '#indexpjax'});
                                        toastr.success('$delete');
                                        swal({
                                        title: '$sweetdeletetitle1',
                                        text: '$sweetdeletetext1',
                                        type: 'success',
                                        confirmButtonClass: 'btn-primary btn-rounded btn-outline',
                                        });
                                    });
                                });
                            ",
                        ]);
                    },
                ]
            ],
        ];

        $fullExportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_BLANK,
            'fontAwesome' => true,
            'filename' => date('Ymdhis').'_'.$this->title,
            'columnSelectorOptions'=>[
                'label' => 'Columns',
                'class' => 'btn btn-default btn-sm btn-rounded btn-outline'
            ],
            'dropdownOptions' => [
                'label' => 'Full',
                'class' => 'btn btn-default btn-sm btn-rounded btn-outline',
                'itemsBefore' => [
                    '<li class="dropdown-header">Export All Data</li>',
                ],
            ],
            'pjaxContainerId' => 'kv-pjax-container',
        ]);
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'columns' => $gridColumns,
        'toolbarContainerOptions' => ['class' => 'btn-toolbar kv-grid-toolbar toolbar-container float-left'],
        'pjax' => true,
        'panel' => [
            // 'type' => GridView::TYPE_SUCCESS,
            'heading' => '<i class="fa fa-bars"></i> '.Yii::t('titlepanel', 'list'),
        ],
        // set a label for default menu
        'export' => [
            'label' => 'Page',
            'fontAwesome' => true,
            'options' => ['class' => 'btn btn-default btn-sm btn-rounded btn-outline'],
        ],
        'toolbar' => [
            $fullExportMenu,
            '{export}',
        ],
        'responsive' => true,
        'responsiveWrap' => false,    
        
    ]); ?>
    <?php
        Modal::begin([
            'title' => Yii::t('modaltitle','CreateUsers'),
            'id' => 'modalCreate',
            'size' => 'modal-md',
        ]);
        echo "<div id='modalCreateContent'></div>";
        Modal::end();

        Modal::begin([
            'title' => Yii::t('modaltitle','UpdateUsers'),
            'id' => 'modalUpdate',
            'size' => 'modal-md',
        ]);
        echo "<div id='modalUpdateContent'></div>";
        Modal::end();

        Modal::begin([
            'title' => Yii::t('modaltitle','UpdateStaffRole'),
            'id' => 'modalView',
            'size' => 'modal-md',
        ]);
        echo "<div id='modalViewContent'></div>";
        Modal::end();
    ?>
    <?php Pjax::end(); ?>
</div>
