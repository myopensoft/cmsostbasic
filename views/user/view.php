<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\User */

$NotifyMsg = Yii::$app->params['successUpdate'];
$jsBegin = <<< JS
    $('.chosen-select').chosen({
        width: "100%",
    });
    $("form").submit(function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var form = $(this);
        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: new FormData(form[0]),
            dataType: 'json',
            contentType: false,
            processData: false,
            async: true,
            success: function(data) {
                console.log(data);
                $('#modalCreate').modal('hide');
                $('#modalUpdate').modal('hide');
                $('#modalView').modal('hide');
                setTimeout(function() {
                    toastr.success('$NotifyMsg');
                    $.pjax.reload({container: '#jquerypjax'});
                    $.pjax.reload({container: '#indexpjax'});
                }, 1000);
            },
            error: function(data) {
                var er;
                $.each(data['responseJSON'], function (key, value) {
                   er += value[0] + '\\n';
                });
                swal({
                    title: er.substr(9),
                    text: '',
                    type: 'warning',
                    confirmButtonText: 'OK',
                    confirmButtonClass: 'btn-primary btn-rounded btn-outline',
                });
//                console.log(data.responseJSON);
            }  
        })
    });
JS;
$this->registerJs($jsBegin);
?>
<div class="user-view">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($authAssignment, 'item_name')->dropDownlist($authItems, ['class' => 'form-control input-sm chosen-select', 'multiple'=>'multiple'])->label('Role'); ?>

    <div class="form-group" align="center">
        <?= Html::submitButton('Update', [
            'class' => $authAssignment->isNewRecord ? 'btn btn-success btn-sm btn-rounded' : 'btn btn-primary btn-sm btn-rounded',
            //'data-confirm'=>"Apakah anda yakin akan menyimpan data ini?",
        ]) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
