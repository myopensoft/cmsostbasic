<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\touchspin\TouchSpin;
use app\models\Ref;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\User */
/* @var $form yii\widgets\ActiveForm */

$create = Yii::$app->params['successCreate'];
$update = Yii::$app->params['successUpdate'];
$delete = Yii::$app->params['successDelete'];
if($model->isNewRecord) {
    $toastr = $create;
}else{
    $toastr = $update;
}
$jsBegin = <<< JS
    $('.select2').select2({
        width: "100%",
    });
    $("form").submit(function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        var form = $(this);
        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: new FormData(form[0]),
            dataType: 'json',
            contentType: false,
            processData: false,
            async: true,
            success: function(data) {
                console.log(data);
                $('#modalCreate').modal('hide');
                $('#modalUpdate').modal('hide');
                setTimeout(function() {
                    toastr.success('$toastr');
                    $.pjax.reload({container: '#jquerypjax'});
                    $.pjax.reload({container: '#indexpjax'});
                }, 1000);
            },
            error: function(data) {
                var er;
                $.each(data['responseJSON'], function (key, value) {
                   er += value[0] + '\\n';
                });
                swal({
                    title: er.substr(9),
                    text: '',
                    type: 'warning',
                    confirmButtonText: 'OK',
                    confirmButtonClass: 'btn-primary btn-rounded btn-outline',
                });
//                console.log(data.responseJSON);
            }  
        })
    });
JS;
$this->registerJs($jsBegin);

?>
<div class="user-form">

    <!-- <div class="row text-center">
        <img alt="image" src="<?php //= MaklumatPeribadi::ShowLightAvatarByUser($model->maklumatPeribadi['nomykad']); ?>" style="width:150px;height:auto">
    </div> -->

    <?php $form = ActiveForm::begin([
        'id' => 'user-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}{beginWrapper}{input}{hint}{error}{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 col-form-label',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'username', [
        'template' => "{label}\n{beginWrapper}\n<p class='form-control-static p-xxs no-margins'>".$model->username."</p>\n{endWrapper}",
    ]); ?>

    <?= $form->field($model, 'full_name')->textInput(['class' => 'form-control input-sm']) ?>

    <?= $form->field($model, 'email')->textInput(['class' => 'form-control input-sm']) ?>

    <?= $form->field($model, 'status')->widget(SwitchInput::classname(), [
        'containerOptions' => [], 
        'pluginOptions' => [
                'size' => 'small',
                'handleWidth'=>80,
                'onText' => Ref::getDesc(39,1),
                'offText' => Ref::getDesc(39,0),
                'onColor' => 'primary',
                'offColor' => 'danger',
        ]]);
    ?>

    
    <div class="form-group" align="center">
        <?= Html::submitButton(Yii::t('btn', 'Save'), ['class' => 'btn btn-success btn-sm']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
