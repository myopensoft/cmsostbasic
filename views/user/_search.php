<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\widgets\SwitchInput;
use app\models\Ref;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
$jsBegin = <<< JS
    $('.select2').select2({
        width: "100%",
    });
JS;
$this->registerJs($jsBegin);
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
        'fieldConfig' => [
            'template' => "{label}{beginWrapper}{input}{hint}{error}{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3 col-form-label',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-9',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class='row'>
        <div class='col-sm-6'>
            <?= $form->field($model, 'username')->textInput(['class' => 'form-control input-sm', 'maxlength' => true]) ?>
            
        </div>
        <div class='col-sm-6'>
            <?= $form->field($model, 'status')->dropDownlist(Ref::GetList(39), ['class' => 'form-control input-sm select2']) ?>
            <?= $form->field($model, 'role')->dropDownlist(User::ListRole(), ['class' => 'form-control input-sm select2']) ?>
        </div>
    </div>
    
    <div class="form-group" align="center">
        <?= Html::submitButton(Html::tag('i', '', ['class' => 'fa fa-search']).' '.Yii::t('btn', 'Search'), ['class' => 'btn btn-sm btn-primary btn-rounded btn-outline']) ?>
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-sync-alt']).' '.Yii::t('btn', 'Reset'), ['index'], ['class' => 'btn btn-sm btn-warning btn-rounded btn-outline']) ?>
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
