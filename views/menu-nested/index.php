<?php

use kartik\tree\TreeView;
use kartik\tree\Module;
use app\models\MenuNested;

$this->title = Yii::t('app', 'Manage Menu');
$this->params['breadcrumbs'][] = ['label' => Yii::t('breadcrumbs', 'Administrator')];
$this->params['breadcrumbs'][] = $this->title;
 
echo TreeView::widget([
    // single query fetch to render the tree
    // use the Product model you have in the previous step
    'query' => MenuNested::find()->addOrderBy('root, lft'),
    'headingOptions' => ['label' => 'Store'],
    'rootOptions' => ['label'=>'<span class="text-primary">Menu</span>'],
    'topRootAsHeading' => true, // this will override the headingOptions
    'allowNewRoots' => true,
    'nodeAddlViews' => [
        Module::VIEW_PART_2 => '@app/views/menu-nested/_treePart2'
    ],
    'headingOptions' => ['label' => 'Menu'],
    'fontAwesome' => true,     // optional
    'isAdmin' => true,         // optional (toggle to enable admin mode)
    'displayValue' => 1,        // initial display value
    'softDelete' => true,       // defaults to true
    'cacheSettings' => [        
        'enableCache' => true   // defaults to true
    ]
]);
