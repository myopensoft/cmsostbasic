<?= $form->field($node, 'menu_loc')->textInput(['class' => 'form-control input-sm', 'maxlength' => true]) ?>

<?= $form->field($node, 'controller_name')->textInput(['class' => 'form-control input-sm', 'maxlength' => true]) ?>

<?= $form->field($node, 'action_name')->textInput(['class' => 'form-control input-sm', 'maxlength' => true]) ?>