<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'm-t']
    ]); ?>
        <?= $form->field($model, 'username')->textInput(['class' => 'form-control', 'autofocus' => true, 'placeholder' => "Username"])->label(false) ?>

        <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control', 'placeholder' => "Password"])->label(false) ?>

        <?= $form->field($model, 'rememberMe')->checkbox() ?>

        <?= Html::submitButton('Login', ['class' => 'btn btn-primary block full-width m-b', 'name' => 'login-button']) ?>

    <?php ActiveForm::end(); ?>