<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $sourcePath = '@app/themes/inspinia';

    public $css = [
        'font-awesome/css/fontawesome-all.min.css',
        'css/plugins/sweetalert/sweetalert.css',
        'css/plugins/ladda/ladda-themeless.min.css',
        'css/style.css',
    ];
    public $js = [
        'js/inspinia.js',
        'js/plugins/metisMenu/jquery.metisMenu.js',
        'js/plugins/sweetalert/sweetalert.min.js',
        'js/plugins/pace/pace.min.js',
        'js/plugins/ladda/spin.min.js',
        'js/plugins/ladda/ladda.min.js',
        'js/plugins/ladda/ladda.jquery.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
