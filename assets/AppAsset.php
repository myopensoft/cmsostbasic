<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@app/themes/inspinia';

    public $css = [
        'font-awesome/css/fontawesome-all.min.css',
        'css/plugins/toastr/toastr.min.css',
        'css/plugins/chosen/bootstrap-chosen.css',
        'css/plugins/select2/select2.min.css',
        'css/plugins/sweetalert/sweetalert.css',
        'css/plugins/ladda/ladda-themeless.min.css',
        'css/style.css',
    ];
    public $js = [
        'js/popper.min.js',
        'js/bootstrap.js',
        'js/inspinia.js',
        'js/plugins/metisMenu/jquery.metisMenu.js',
        'js/plugins/slimscroll/jquery.slimscroll.min.js',
        'js/plugins/toastr/toastr.min.js',
        'js/plugins/chosen/chosen.jquery.js',
        'js/plugins/select2/select2.full.min.js',
        'js/plugins/sweetalert/sweetalert.min.js',
        'js/plugins/pace/pace.min.js',
        'js/plugins/ladda/spin.min.js',
        'js/plugins/ladda/ladda.min.js',
        'js/plugins/ladda/ladda.jquery.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
