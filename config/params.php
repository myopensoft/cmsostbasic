<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'bsVersion' => '4',
    'confirmDelete' => 'Are you sure to delete this record?',
    'successCreate' => 'Records have been <b>SUCCESSFULLY CREATED</b>',
    'successUpdate' => 'Records have been <b>SUCCESSFULLY UPDATED</b>',
    'successDelete' => 'Records have been <b>SUCCESSFULLY DELETED</b>',
    'errorDelete' => 'File <b>NOT FOUND</b>',
];
