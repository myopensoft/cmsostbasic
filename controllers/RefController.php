<?php

namespace app\controllers;

use Yii;
use app\models\Ref;
use app\models\RefSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class RefController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'delete-param' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndexCat() {
        $searchModel = new RefSearch();
        $searchModel->cat = 'CAT';
        $dataProvider = $searchModel->searchCat(Yii::$app->request->queryParams);

        return $this->render('index_cat', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexParam($id) {
        $searchModel = new RefSearch();
        $searchModel->cat = $id;
        $dataProvider = $searchModel->searchParam(Yii::$app->request->queryParams);

        return $this->renderAjax('index_param', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new Ref();

        if ($model->load(Yii::$app->request->post())) {
            $model->cat = 'CAT';
            $model->status = '1';
            if ($model->save()) {
                Ref::updateAll(['code' => $model->id], 'id = ' . $model->id);
                return true;
            }else{
                Yii::$app->response->statusCode = 404;
                return json_encode($model->errors);
            }
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    public function actionCreateParam($cat) {
        $model = new Ref();
        $model->status = '1';
        
        if ($model->load(Yii::$app->request->post())) {
            $model->cat = $cat;
            if($model->save()) {
                return $this->asJson([
                    'status' => 'OK',
                    'id' => $model->cat,
                ]);
            }else{
                Yii::$app->response->statusCode = 404;
                return json_encode($model->errors);
            }
        }

        return $this->renderAjax('createparam', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) {
                return true;
            }else{
                Yii::$app->response->statusCode = 404;
                return json_encode($model->errors);
            }
        }
            
        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }
    
    public function actionUpdateParam($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) {
                return $this->asJson([
                    'status' => 'OK',
                    'id' => $model->cat,
                ]);
            }
        }
        
        return $this->renderAjax('updateparam', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Ref::deleteAll('cat = :cat', [':cat' => $id]);
        return true;
    }
    
    public function actionDeleteParam($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        return $this->asJson([
            'status' => 'OK',
            'id' => $model->cat,
        ]);
    }

    protected function findModel($id) {
        if (($model = Ref::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
