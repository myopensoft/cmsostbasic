<?php

namespace app\controllers;

use Yii;
use app\models\SourceMessage;
use app\models\SourceMessageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\MessageSearch;
use app\models\Message;

/**
 * TranslationController implements the CRUD actions for SourceMessage model.
 */
class TranslationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionChangeLang($lang)
    {
        Yii::$app->session->set('language', $lang);
        return true;
    }

    /**
     * Lists all SourceMessage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SourceMessageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionMessage($id)
    {
        $searchModel = new MessageSearch();
        $searchModel->id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('message', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id
        ]);
    }

    /**
     * Displays a single SourceMessage model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SourceMessage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SourceMessage();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) {
                return true;
            }else{
                Yii::$app->response->statusCode = 400;
                return json_encode($model->errors);
            }
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }
    
    public function actionCreateMessage($id)
    {
        $model = new Message();
        $model->id = $id;

        if ($model->load(Yii::$app->request->post())) {
            $model->id = $id;
            if($model->save()) {
                return $this->asJson([
                    'status' => 'OK',
                    'id' => $model->id,
                ]);
            }else{
                Yii::$app->response->statusCode = 400;
                return json_encode($model->errors);
            }
        }

        return $this->renderAjax('create_message', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SourceMessage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) {
                return true;
            }else{
                Yii::$app->response->statusCode = 400;
                return json_encode($model->errors);
            }
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdateMessage($id,$language)
    {
        $model = Message::find()->where(['id' => $id, 'language' => $language])->one();

        if (Yii::$app->request->post()) {
            Message::updateAll(['translation' => Yii::$app->request->post('Message')['translation']], "id = {$id} AND language = '{$language}'");
            return $this->asJson([
                'status' => 'OK',
                'id' => $id,
            ]);
        }

        return $this->renderAjax('update_message', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SourceMessage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['index']);
        }else{
            return true;
        }
    }

    public function actionDeleteMessage($id,$language)
    {
        Yii::$app->db->createCommand()->delete('message', 'id = :id AND language = :language')
        ->bindValue(':id', $id)
        ->bindValue(':language', $language  )
        ->execute();

        return $this->asJson([
            'status' => 'OK',
            'id' => $id,
        ]);
    }

    /**
     * Finds the SourceMessage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SourceMessage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SourceMessage::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
